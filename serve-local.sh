# running on 8001 to avoid a conflict with Plausible instances
open http://localhost:8001/
mkdocs serve --watch-theme --livereload --dev-addr localhost:8001 