// modified from Google`s guide at: https://developers.google.com/web/fundamentals/primers/service-worker
// and https://gist.github.com/JMPerez/8ca8d5ffcc0cc45a8b4e1c279efd8a94
const VERSION = `0.0.2`;
const CACHE_NAME = `ASERG-SHU-DEV-${VERSION}`;
//these are files that are never expected to change
const base = "http://aserg.codeberg.page/shu-dev-process";
const alwaysCached = [
    base,
    //built-in theme files
    `${base}/search/main.js`,
    `${base}/css/theme.css`,
    `${base}/css/theme_extra.css`,
    `${base}/js/theme.js`,
    `${base}/js/theme_extra.js`,
    `${base}/js/jquery-2.1.1.min.js`,
    `${base}/js/modernizr-2.8.3.min.js`,
    `${base}/search/worker.js`,
    `${base}/search/main.js`,
    `${base}/fonts/Lato/lato-bold.woff2`,
    `${base}/fonts/Lato/lato-italic.woff2`,
    `${base}/fonts/Lato/lato-regular.woff2`,
    `${base}/fonts/fontawesome-webfont.woff2?v=4.7.0`,
    //extra files
    `${base}/img/favicon.ico`,
    `${base}/css/mermaid.css`,
    `${base}/js/mermaid.min.js`,
    `${base}/fonts/FiraCode-variableFont.ttf`,
    `${base}/fonts/FS Clerkenwell Bold.otf`,
    `${base}/fonts/FS Clerkenwell Italic.otf`,
    `${base}/fonts/FS Clerkenwell Light.otf`,
    `${base}/fonts/FS Clerkenwell.otf`,
];

// on activation we clean up the previously registered service workers
self.addEventListener(`activate`, evt =>
    evt.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames.map(cacheName => {
                    if (cacheName !== CACHE_NAME) {
                        return caches.delete(cacheName);
                    }
                })
            );
        })
    )
);

// on install we download the routes we want to cache for offline use
self.addEventListener(`install`, evt =>
    evt.waitUntil(
        caches.open(CACHE_NAME).then(cache => {
            return cache.addAll(alwaysCached);
        })
    )
);

// cache the current page to make it available for offline
const update = request =>
    caches
    .open(CACHE_NAME)
    .then(cache =>
        fetch(request).then(response => cache.put(request, response))
    );

// fetch the resource from the network
const fromNetwork = (request, timeout) =>
    new Promise((fulfill, reject) => {
        const timeoutId = setTimeout(reject, timeout);
        fetch(request).then(response => {
            clearTimeout(timeoutId);
            fulfill(response);
            update(request);
        }, reject);
    });

// fetch the resource from the browser cache
const fromCache = request =>
    caches
    .open(CACHE_NAME)
    .then(cache =>
        cache
        .match(request)
        .then(matching => matching || cache.match(`/offline/`))
    );

// cache the current page to make it available for offline
const update = request =>
    caches
    .open(CACHE_NAME)
    .then(cache =>
        fetch(request).then(response => cache.put(request, response))
    );

// strategy when making a request is network first
self.addEventListener(`fetch`, evt => {
    let networkThenCache = () => fromNetwork(evt.request, 5000).catch(() => fromCache(evt.request));
    evt.respondWith(
        networkThenCache()
    );
    evt.waitUntil(update(evt.request));
});