The files in this folder follow the structure of the upstream built-in [`readthedocs` theme](https://github.com/mkdocs/mkdocs/tree/master/mkdocs/themes/readthedocs).

They'll overwrite the default files with our own styles and changes, per mkdocs' [`custom_dir` parameter](https://www.mkdocs.org/user-guide/customizing-your-theme/#using-the-theme-custom_dir).

Mkdocs uses [Jinja](https://jinja.palletsprojects.com/) to build templates. Read more about customising themes [here](https://www.mkdocs.org/user-guide/customizing-your-theme/).

In order to maintain upstream changes to the base `readthedocs` theme, changes should only be made inside the `main.html` file.

