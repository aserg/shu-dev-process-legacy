const linkToImgs = () => {
    let elems = document.querySelectorAll("img");
    if (elems.length) {
        elems.forEach(imgEl => {
            imgEl.loading = "lazy";
            if (!imgEl.classList.contains("no-original")) {
                let a = document.createElement("a");
                a.href = imgEl.src;
                a.target = "_blank";
                a.appendChild(imgEl.cloneNode());
                let text = document.createElement("i");
                text.innerText = "view original";
                text.classList.add("view-original-link");
                a.appendChild(text);
                imgEl.after(a);
                imgEl.remove();
            }
        });
    }
}

linkToImgs();