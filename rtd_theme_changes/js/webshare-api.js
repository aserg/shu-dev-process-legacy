//copy the current link to the clipboard
const copyLinkToClipboard = (evt) => {
    evt.preventDefault();
    let el = evt.target;
    let url = el.href;
    //fill the input field so that it can be selected
    let copyInput = document.getElementById("clipboardCopyInput");
    copyInput.value = url;
    copyInput.select();
    copyInput.setSelectionRange(0, 99999); //for mobile devices
    /* Copy the text inside the text field */
    navigator.clipboard.writeText(copyInput.value);
    //add a class to say that it was copied
    //remove that class after a timeout
    el.parentElement.classList.add("copied-text");
    setTimeout(() => {
        el.parentElement.classList.remove("copied-text")
    }, 750);
}


//format text for a .headerlink anchor sharing title
//gets the text of the heading this link belongs to and adds this to the document title
//includes the level number if one is present
const makeShareTitle = (el) => {
        let headingText = el.parentElement.textContent.replace("#", "").trim();
        //remove numbers from the sharing title
        let regex = new RegExp("^(([0-9]{1,}(\.)?){1,3})", "gm");
        let match = headingText.match(regex);
        if (match && match.length) {
            headingText = headingText.replace(match[0], "").trim();
        }
        //ensure the level number is included in the sharing title if it hasn't already come from the headingText or document.title
        let levelMatch = el.href.match(new RegExp("(?<=level-)[0-9]{1}", "gm")); //extract number from URL
        let levelNumber = levelMatch ? levelMatch[0] : null;
        let levelStr = `Level ${levelNumber}`;
        let alreadyIncludesLevel = levelNumber && (headingText.toLowerCase().includes(levelStr.toLowerCase()) || document.title.toLowerCase().includes(levelStr.toLowerCase()));
        return `${headingText} ${levelMatch && !alreadyIncludesLevel ? `|  ${levelStr}` : ""} | ${document.title}`;
};

//trigger the OS sharing action
const shareAPIAction = (evt) => {
    evt.preventDefault();
    console.log("share", evt);
    let el = evt.target;
    let shareData = {
        title: makeShareTitle(el),
        text: makeShareTitle(el),
        url: el.href
    };
    if (navigator.share && navigator.canShare(shareData)) {
        navigator.share(shareData)
            .then(() => {
                console.log('OS Share UI triggered');
                //add a class to say that it was shared
                //remove that class after a timeout
                el.parentElement.classList.add("shared-text");
                setTimeout(() => {
                    el.parentElement.classList.remove("shared-text")
                }, 750);
            }
            )
            .catch((error) => console.log('Error sharing', error));
    }
}

/**
 * If the WebShare API is available, upgrade header links to use this instead of just allowing the user to copy the link
 */
const addSharingLinks = () => {
    let hiddenInput = document.createElement("input");
    hiddenInput.className = "hidden";
    hiddenInput.id = "clipboardCopyInput";
    document.body.appendChild(hiddenInput);
    document.querySelectorAll("a.headerlink").forEach(el => {
        let shareData = {
            title: makeShareTitle(el),
            text: makeShareTitle(el),
            url: el.href
        };
        let canShare = navigator.share && navigator.canShare(shareData);
        el.classList.add(canShare ? "headerlink-share" : "headerlink-copy");
        el.title = canShare ? "Share" : "Copy";
        el.addEventListener("click", canShare ? shareAPIAction : copyLinkToClipboard);
    });
}

addSharingLinks();