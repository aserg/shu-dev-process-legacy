//replace `mermaid` language code blocks with a div that mermaid js will recognise, for easier document creation
function initMermaid() {
    return new Promise((resolve, reject) => {
        let elems = document.querySelectorAll("code.language-mermaid");
        if (elems.length) {
            elems.forEach(el => {
                let div = document.createElement("div");
                div.className = "mermaid";
                div.innerHTML = el.innerHTML;
                el.replaceWith(div);
            })
            mermaid.initialize({
                startOnLoad: true,
            });
        }
    })
}

initMermaid();