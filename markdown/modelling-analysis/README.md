# Analysis <!-- omit in toc -->

## 1. Introduction

The analysis phase is dedicated to the formalisation of the requirements elicitation.
It usually involve talking with a client and documenting your understanding.
Amongst the artefact used we can mention user stories and use cases.
These are also used to help organise, prioritise and divide the workload.

## 2. Table of Contents

- [1. Introduction](#1-introduction)
- [2. Table of Contents](#2-table-of-contents)
- [3. Level Specific Guidance](#3-level-specific-guidance)
    - [Level 4](#level-4)
    - [Level 5](#level-5)
    - [Level 6](#level-6)

## 3. Level Specific Guidance

### Level 4

[Level 4 User Stories](level-4/level-4-user-stories.md)

[Level 4 Use Case Guidance](level-4/level-4-use-case-guidance.md)

### Level 5

[Level 5 User Stories](level-5/level-5-user-stories.md)

[Level 5 Use Case Guidance](level-5/level-5-use-case-guidance.md)

[Level 5 Personas and User Scenarios](level-5/personas-and-scenarios.md)

[MoSCoW Prioritisation Method](level-5/moscow-prioritisation-method.md)

### Level 6

[Level 6 User Stories](level-6/level-6-user-stories.md)