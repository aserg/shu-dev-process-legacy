# Software Development Process <!-- omit in toc -->

- [1. Introduction](#1-introduction)
- [2. The Reference Process](#2-the-reference-process)
- [3. Level-based view](#3-level-based-view)
    - [3.1. Level 4](#31-level-4)
    - [3.2. Level 5](#32-level-5)
    - [3.3. Level 6](#33-level-6)
- [4. Process-based view](#4-process-based-view)
    - [4.1. Initiation](#41-initiation)
    - [4.2. Planning](#42-planning)
    - [4.3. Modelling: Analysis](#43-modelling-analysis)
    - [4.4. Modelling: Design](#44-modelling-design)
    - [4.5. Construction: Code](#45-construction-code)
    - [4.6. Construction: Test](#46-construction-test)
    - [4.7. Deployment: Delivery](#47-deployment-delivery)
    - [4.8. Deployment: Support and Feedback](#48-deployment-support-and-feedback)
- [5. Tools view](#5-tools-view)
- [6. Development](#6-development)
    - [6.1 Team](#61-team)
    - [6.2 License](#62-license)
    - [6.3 Analytics](#63-analytics)

## 1. Introduction

We are creating a software development process that can be followed by students at all levels as they work on both individual and group
assessments.
We're doing this because we want our students to use good practice, to understand the software lifecycle and to become familiar with industry-standard tools.

## 2. The Reference Process

The SHU development process is based on the generic process framework described by Pressman and Maxim [REF], and comprises five methodological stages: initiation, planning, modelling, construction and deployment. These main stages encompass a set of support steps: analysis and design related to modelling; code and test related to construction; and delivery, support and feedback for deployment.

![Reference Process](./Process.webp)

Each level of study has an specific set of guidelines, techniques and tools with appropriate depthness for the level.

The different stages are presented below, with links for their respective guidelines and can be viewed in two ways:

- Click [here](#3-level-based-view) for a level-based view of the guidelines ([L4](#31-level-4), [L5](#32-level-5), [L6](#33-level-6)).
- Click [here](#4-process-based-view) for a process-based view of the guidelines.

Rembember, these stages are simply used for helping organising and presenting the process, and should not be interpreted as a strict order.
In fact, each student is free to navigate over the stages as he/she sees fit.

A description of each phase is presented below.

- The **Initiation** phase is dedicated to understanding the objectives of stakeholders, and for the design and collection of requirements that help identify the expected software functionalities and quality attributes.
- The **Planning** phase is responsible for defining the scope of the project, the resources needed, the risks involved, a work schedule, and the resulting products.
- The **Modelling** phase is divided into analysis and design steps.
  - The **Analysis** is focused on the definition/formalisation of requirements, using artefacts like user stories and use cases.
  - The **Design** step is focused on the modelling of the solution. It usually involves decision around architecture, data structure, user-interface, classes, algorithms, etc.
- The **Construction** phase involves the implementation of the structures modeled in the design phase. For this purpose, **code** and **tests** are conducted.
- The **Deployment** involves the steps of **delivery** and **support and feedback**.
  - **Delivery** is focused activities associated with the ongoing DevOps practices.
  - **Support and feedback** step is driven by monitoring and maintenance activities. This step also contains guidance regarding assessment, reports and presentations. We also consider evaluation, reporting and reflection as part of this last step.

## 3. Level-based view

### 3.1. Level 4

- Initiation
    - [Initiation Guidelines](initiation/level-4/level4-initiation.md)
- Planning
    - [Git Version Control](planning/version-control/level-4-git-instructions.md)
    - [Project Management](planning/project-management/level-4/level-4-management-guidelines.md)
    - [Team Communication](planning/team-communication/level-4/level-4-team-communication-guidelines.md)
- Modelling - Analysis
    - [User Stories](modelling-analysis/level-4/level-4-user-stories.md)
    - [Use Case Models](modelling-analysis/level-4/level-4-use-case-guidance.md)
- Modelling - Design
    - [Design](modelling-design/level-4/level4-design.md)
    - [Class Diagrams](modelling-design/level-4/level4-UML-class-diagram.md)
    - [Entity Relationship Diagrams](modelling-design/level-4/level4-entity-relationship-diagram.md)
    - [Sequence Diagrams](modelling-design/level-4/level4-sequence-diagram.md)
- Construction - Code
    - [Code Commenting](construction-code/level-4/level4-code-commenting.md)
    - [Code Review](construction-code/code-review/level-4/level-4-code-review-guidelines.md)
- Construction - Test
    - [Testing Guidelines](construction-test/level-4/level4-testing.md)
- Deployment - Delivery
    - [Delivery Guidelines](deployment-delivery/level-4/level-4-delivery-guidelines.md)
- Deployment - Support - Feedback
    - [Presenting, Reporting and Demonstrating](deployment-support-feedback/level-4/level-4-feedback-guidelines.md)
  
### 3.2. Level 5

- Initiation
    - [Initiation Guidelines](initiation/level-5/level5-initiation.md)
- Planning
    - [Git Version Control](planning/version-control/level-5-git-instructions.md)
    - [Project Management](planning/project-management/level-5/level-5-management-guidelines.md)
    - [Team Communication](planning/team-communication/level-5/level-5-team-communication-guidelines.md)
- Modelling - Analysis
    - [User Stories](modelling-analysis/level-5/level-5-user-stories.md)
    - [Use Case Models](modelling-analysis/level-5/level-5-use-case-guidance.md)
    - [Personas and Scenarios](modelling-analysis/level-5/personas-and-scenarios.md)
    - [MoSCoW Prioritisation Method](modelling-analysis/level-5/moscow-prioritisation-method.md)
- Modelling - Design
    - [Design](modelling-design/level-5/level5-design.md)
    - [Class Diagrams](modelling-design/level-5/level5-UML-class-diagram.md)
    - [Entity Relationship Diagrams](modelling-design/level-5/level5-entity-relationship-diagram.md)
    - [Sequence Diagrams](modelling-design/level-5/level5-sequence-diagram.md)
- Construction - Code
    - [Code Commenting](construction-code/level-5/level5-code-commenting.md)
    - [Code Review](construction-code/code-review/level-5/level-5-code-review-guidelines.md)
- Construction - Test
    - [Testing Guidelines](construction-test/level-5/level5-testing.md)
- Deployment - Delivery
    - [Delivery Guidelines](deployment-delivery/level-5/level-5-delivery-guidelines.md)
- Deployment - Support - Feedback
    - [Presenting, Reporting and Demonstrating](deployment-support-feedback/level-5/level-5-feedback-guidelines.md)

### 3.3. Level 6

- Initiation
    - [Initiation Guidelines](initiation/level-6/level6-initiation.md)
- Planning
    - [Git Version Control](planning/version-control/level-6-git-instructions.md)
    - [Project Management](planning/project-management/level-6/level-6-management-guidelines.md)
    - [Team Communication](planning/team-communication/level-6/level-6-team-communication-guidelines.md)
- Modelling - Analysis
    - [User Stories](modelling-analysis/level-6/level-6-user-stories.md)
- Modelling - Design
    - [Design](modelling-design/level-6/level6-design.md)
    - [Entity Relationship Diagrams](modelling-design/level-6/level6-entity-relationship-diagram.md)
    - [Sequence Diagrams](modelling-design/level-6/level6-sequence-diagram.md)
- Construction - Code
    - [Code Commenting](construction-code/level-6/level6-code-commenting.md)
    - [Code Review](construction-code/code-review/level-6/level-6-code-review-guidelines.md)
- Construction - Test
    - [Testing Guidelines](construction-test/level-6/level6-testing.md)
- Deployment - Delivery
    - [Delivery Guidelines](deployment-delivery/level-6/level-6-delivery-guidelines.md)
- Deployment - Support - Feedback
    - [Presenting, Reporting and Demonstrating](deployment-support-feedback/level-6/level-6-feedback-guidelines.md)

## 4. Process-based view

### 4.1. Initiation

- [**Initiation guidelines**](initiation/README.md)

### 4.2. Planning

- [**Planning guidelines**](planning/README.md)

### 4.3. Modelling: Analysis

- [**Analysis guidelines**](modelling-analysis/README.md)

### 4.4. Modelling: Design

- [**Design guidelines**](modelling-design/README.md)

### 4.5. Construction: Code

- [**Code guidelines**](construction-code/README.md)

### 4.6. Construction: Test

- [**Tests guidelines**](construction-test/README.md)

### 4.7. Deployment: Delivery

- [**Delivery guidelines**](deployment-delivery/README.md)

### 4.8. Deployment: Support and Feedback

- [**Support and feedback guidelines**](deployment-support-feedback/README.md)

## 5. Tools view

The below are given as suggestions only, you may find and use other tools that you prefer.

- [**Project Management:**](planning/project-management/tools/project-management-tools.md) [Trello](https://trello.com/), [MS Project](https://www.microsoft.com/en-gb/microsoft-365/project/project-management-software), MS Excel
- **Team Communication:** [Teams](https://www.microsoft.com/en-gb/microsoft-teams/log-in), [Slack](https://slack.com/)
- [**Version Control:**](planning/version-control/tools/level-4-git-tools.md) Git, CVS or similar
- **Design:** [diagrams.net](https://www.diagrams.net/),[STARUML](https://staruml.io/), [Visio](https://www.microsoft.com/en-gb/microsoft-365/visio/flowchart-software),[mermaid-js](https://mermaid-js.github.io/mermaid/#/),[TextUML](http://abstratt.github.io/textuml/) or similar
- **Coding:** Visual Studio / Intelli J  or similar
- **Unit Testing:** [Junit](https://junit.org/junit5/)/[Nunit](https://nunit.org/),[QUnit](https://qunitjs.com/), [Mocha](https://mochajs.org/) or similar.
- **Code Review:** [SonarQube](https://www.sonarqube.org/)
- **Integration Testing:** [Azure DevOps](https://azure.microsoft.com/en-us/services/devops/) or similar
- **User Acceptance Testing:** [Selenium](https://www.selenium.dev/)
- **Deployment/Release:** [Azure DevOps](https://azure.microsoft.com/en-us/services/devops/) or similar

## 6. Development
###  6.1 Team

- 2022 Jack Carey <jack.carey@student.shu.ac.uk>
- 2020 - 2022 Carlos Da Silva <c.dasilva@shu.ac.uk>
- 2020 - 2021 Soumya Basu <soumya.basu@shu.ac.uk>
- 2020 - 2021 Chris Bates
- 2020 - 2021 James Harmson
- 2020 - 2021 Brian Davis

### 6.2 License

This material is made available under [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

### 6.3 Analytics

We are using privacy preserving analytics from [Plausible.io](https://plausible.io/privacy-focused-web-analytics) to monitor which pages students view and the terms they search for. The analytics we collect are completely anonymous and no cookies are stored in your browser. Nevertheless, we're letting you know that we will use the information to inform development of the content on these pages.

Please contact the current research team if you have any questions.