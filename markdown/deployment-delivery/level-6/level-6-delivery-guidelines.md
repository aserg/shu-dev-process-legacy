# Level 6 Delivery Guidelines <!-- omit in toc -->

## 1. Abstract

This document will serve to provide Level 6 students with instructions on what you need to do for arguably the most important phase of the software development lifecycle. The Delivery of the product. At Level 6, along with submitting the README file located [here](../L6-README-Template.md), we expect DevOps culture to be adopted, especially for your Final Year Project, and group assignments.  

## 2. Table of Contents

- [1. Abstract](#1-abstract)
- [2. Table of Contents](#2-table-of-contents)
- [3. DevOps](#3-devops)
- [4. What is Expected](#4-what-is-expected)
    - [4.1 Plan](#41-plan)
    - [4.2 Code & Build](#42-code--build)
    - [4.3 Testing, Release & Deployment](#43-testing-release--deployment)
    - [4.4 Operate](#44-operate)
- [5. Tools](#5-tools)
    - [5.1. Testing](#51-testing)
    - [5.2. Release](#52-release)
    - [5.3. Deployment/Operate](#53-deploymentoperate)
- [6. Examples](#6-examples)
    - [6.1 Continuous Integration on Github Actions](#61-continuous-integration-on-github-actions)
    - [6.2 Continuous Deployment with GitHub Pages](#62-continuous-deployment-with-github-pages)
    - [6.3 Webhooks](#63-webhooks)
            - [Polling (synchronous)](#polling-synchronous)
            - [Webhooks (asynchronous)](#webhooks-asynchronous)
- [7. References](#7-references)

## 3. DevOps

If you have read through the [level 5](../level-5/level-5-delivery-guidelines.md) guidelines, you should now be familiar with what DevOps is, and the different phases involved within the culture.
As a reminder, DevOps (Development Operations) is a set of practices that works to automate and integrate the processes between software development and IT teams, so they can build, test, and release software faster and more reliably (reference [1]).
DevOps is an important aspect of software development to learn before you graduate university, and it will impress your interviewers for any graduate roles/schemes.

DevOps is made up of 8 main practices, which can be seen from this diagram:

![DevOps diagram](../images/devops-diagram.png)
*Fig. 1: DevOps practices. Retrieved from reference [2]*

## 4. What is Expected

At Level 6, we are expecting to see DevOps culture adopted for your larger projects, most notably your Final Year Project. It would also be nice to see in your group assignments. We are expecting each phase to be implemented, other than the final stage; **Monitor**, as you are not expected to maintain these projects after submission (although it may be beneficial to you to maintain your Final Year Projects). A CI/CD pipeline should be adopted at the minimum.

We expect:

### 4.1 Plan

You should be correctly using version control with branching and CI/CD adoption. You're expected to implement good project management and communicate well with your team. And remember, you should always fill in and submit a README file detailing the contents of your submission. This can be found [here](../L6-README-Template.md). You should do this along with any DevOps implementation, the details of this should be outlined in the README.
#### Code Branches <!-- omit in toc -->>

Branching at level 6 is expected to follow professional practice, with five different types of branch:

- `main` - Code is never directly edited on this branch. It is used only to store the release history that you provide to the public/client.
- `develop` - An integration branch for features. While you're working towards a release, new features will be forked to and from this branch.
- `feature` - Feature branches are forked from the `develop` branch. This is where the bulk of your code should be written. 
- `release` - Used for completing a dry-run of your release before it is published publicly or to your client. You can remove small issues and polish your work here. The `develop` branch should be merged to this branch once enough features have been implemented and tested.
- `hotfix` - If a bug is found in production that cannot be fixed in the next release, a hotfix branch can be used to patch it quickly. It is the only time you should fork directly off main. You should merge hotfixes into both `main` and `develop`.

![branch structure](branches.svg)

[Atlassin CC BY 2.5 AU](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow)

By following this structure, you will be able to collaborate effectively with your team and maintain well structured phases of development. You'll be able to track a project's development over time, which becomes even more important as you work on larger projects. When working for a company you may find that branches are restricted so that only certain users can interact with them. This is so that reviews can take place and code quality can be maintained.

### 4.2 Code & Build

You should maintain good quality code and [comments](../../construction-code/level-6/level6-code-commenting.md). We expect to see every [diagram and model](../../modelling-design/README.md) that you have used previously. The code you write should reflect these.

* [UML Class Diagram](level-5/level5-UML-class-diagram.md)
* [Entity Relationship Diagram](level-6/level6-entity-relationship-diagram.md)
* [Sequence Diagrams](level-6/level6-sequence-diagram.md)
* [Architecture Overview](level-6/level6-architecture-overview.md)

### 4.3 Testing, Release & Deployment

At level 6 you should be using testing your applications comprehensively using a tool (see section 5. below). This will include unit tests for specific components, as well as acceptance tests for an entire action/system.

Continuous Integration and Deployment should be adopted, which could be through the use of GitHub Actions, webhooks, or another method. This should include automated tests and frequent deployments.
### 4.4 Operate

While not essential for all assignments, it would be good to see this in your final year project or when working with clients. During the operation stage, you should be monitoring the performance of your application. This means you may need to scale resources, implement channels for users to provide feedback (and act on it!), and watch for usage trends within your system.
## 5. Tools

We recommend looking into the tools below for integrating the full DevOps pipeline into your projects. We are not expecting all of these to be used, but some such as Junit/NUnit and Github Actions would be good to see:

### 5.1. Testing

- JUnit: <https://junit.org/junit5/>
- NUnit: <https://nunit.org/>
- QUnit: <https://qunitjs.com/>
- Mocha: <https://mochajs.org>
- Jest: <https://jestjs.io>
- Selenium: <https://www.selenium.dev/>

### 5.2. Release

- GitHub Actions: <https://github.com/features/actions>
- Gitlab: <https://about.gitlab.com/>
- Gitea: <https://gitea.io/en-us/>
- Codeship: <https://www.cloudbees.com/products/codeship>
- Jenkins: <https://www.jenkins.io/>

### 5.3. Deployment/Operate

- Docker: <https://www.docker.com/>
- Kubernetes: <https://kubernetes.io/>
- Azure: <https://azure.microsoft.com/en-gb/> (free credit available from university)
- DC/OS: <https://dcos.io/>

## 6. Examples

### 6.1 Continuous Integration on Github Actions

This action will install NodeJS and the dependencies your project requires, then build and test based on your `npm test` command. This will trigger only when you push to or make a pull request for your `main` branch. If any of your tests fail then so to will this action. You can even use [workflow badges](https://docs.github.com/en/actions/monitoring-and-troubleshooting-workflows/adding-a-workflow-status-badge) in the readme of your repository to quickly visualise the latest action result.

```yml
name: Node.js CI
on:
  push:
    branches: [ main ]
  pull_request:
    branches: [ main ]
jobs:
  build:
    runs-on: ubuntu-latest 
    strategy:
      matrix:
        node-version: [16.14.x, 17.5.x]
    steps:
    - uses: actions/checkout@v2
    - name: Use Node.js ${{ matrix.node-version }}
      uses: actions/setup-node@v2
      with:
        node-versio n: ${{ matrix.node-version }}
        cache: 'npm'
    - run: npm ci
    - run: npm run build --if-present
    - run: npm test
```

* [View on GitHub Docs](https://docs.github.com/en/actions/automating-builds-and-tests/building-and-testing-nodejs-or-python)

### 6.2 Continuous Deployment with GitHub Pages

GitHub allows you to host static webpages through their Pages platform.
### 6.3 Webhooks

If your system wants to react to changes in another system it is possible to keep polling the system to ask "*has my data changed?*". This is computationally expensive and can use excessive amounts of bandwidth. Instead with webhooks, your system registers a callback URL with the external server that the external server then calls when the data has changed.
##### Polling (synchronous) <!--omit in toc-->

```mermaid
sequenceDiagram
    participant System
    participant External Server
    System->>External Server: Is my data ready?
    External Server->>System: No
    System->>External Server: Is my data ready?
    External Server->>System: No
    System->>External Server: Is my data ready?
    External Server->>System: No
    System->>External Server: Is my data ready?
    External Server->>System: Here's your data
```

##### Webhooks (asynchronous) <!--omit in toc-->

```mermaid
sequenceDiagram
    participant System
    participant External Server
    System->>External Server: Tell me when my data is ready.
    External Server->>System: Here's your data
```

For example, it's possible to receive Discord messages in a server based on events in a Git repository. You can read how to do this on [Discord's documentation](https://support.discord.com/hc/en-us/articles/228383668-Intro-to-Webhooks).

## 7. References

[1] Atlassian. DevOps: Breaking the Development-Operations barrier. <https://www.atlassian.com/devops>.

[2] Medium. How to Become an DevOps Engineer in 2020. <https://medium.com/swlh/how-to-become-an-devops-engineer-in-2020-80b8740d5a52>. By Shane Shown.

