# Planning <!-- omit in toc -->

## 1. Introduction

The planning phase is responsible for defining the scope of the project, the resources needed, the risks involved, a work schedule, and the resulting products.

This document provides some guidelines that can be used to help you in this phase. There are three main components of planning a project. All will be covered in the [Level Specific Guidance](#level-specific-guidance) documents:

- Project Management
- Team Communication
- Version Control

## 2. Table of Contents

- [1. Introduction](#1-introduction)
- [2. Table of Contents](#2-table-of-contents)
- [3. Planning your work](#3-planning-your-work)
    - [Specific](#specific)
    - [Measurable](#measurable)
    - [Achievable/Attainable](#achievableattainable)
    - [Realistic/Relevant](#realisticrelevant)
    - [Time-related](#time-related)
- [Level Specific Guidance](#level-specific-guidance)
    - [Level 4](#level-4)
    - [Level 5](#level-5)
    - [Level 6](#level-6)
- [4. References](#4-references)

## 3. Planning your work

These are some points to be considered as a general guidance when planning your work:

- Development process and tools
  - Communication tool
  - Project management
  - Version control
  - IDE and frameworks
  - Testing
- Team roles: In a group, determine who will play each role
  - Project Manager
  - Leading on Design
  - Overseeing coding
  - Leading Testing
  - Talking to the client
  - any other role
- Feature points
- Resource Management: What do you need to conduct the project? What do you already have?
  - Hardware
  - Software
  - Expertise
- Milestones
  - What are your deadlines?
- Risk assessment
  - What are the risks?
    - internal/external factors that may affect your team's ability to deliver the project.
  - How do you prioritise?
  - Can you mitigate them?

When planning your work you should try to adopt SMART planning:

- **S**: Specific
- **M**: Measurable
- **A**: Achievable
- **R**: Realistic/Relevant
- **T**: Time-related

### Specific

Your goal should be clear and specific, otherwise you won't be able to focus your efforts or feel truly motivated to achieve it. When drafting your goal, try to answer the five "W" questions:

- What do I want to accomplish?
- Why is this goal important?
- Who is involved?
- Where is it located?
- Which resources or limits are involved?

### Measurable

It's important to have measurable goals, so that you can track your progress and stay motivated. Assessing progress helps you to stay focused, meet your deadlines, and feel the excitement of getting closer to achieving your goal.

A measurable goal should address questions such as:

- How much?
- How many?
- How will I know when it is accomplished?

### Achievable/Attainable

Your goal also needs to be realistic and attainable to be successful. In other words, it should stretch your abilities but still remain possible. When you set an achievable goal, you may be able to identify previously overlooked opportunities or resources that can bring you closer to it.

An achievable goal will usually answer questions such as:

- How can I accomplish this goal?
- How realistic is the goal, based on other constraints, such as financial factors?

### Realistic/Relevant

This step is about ensuring that your goal matters to you, and that it also aligns with other relevant goals. We all need support and assistance in achieving our goals, but it's important to retain control over them.

A relevant goal can answer "yes" to these questions:

- Does this seem worthwhile?
- Is this the right time?
- Does this match our other efforts/needs?
- Am I the right person to reach this goal?
- Is it applicable in the current socio-economic environment? (not as necessary at university)

### Time-related

Every goal needs a target date, so that you have a deadline to focus on and something to work toward. This part of the SMART goal criteria helps to prevent everyday tasks from taking priority over your longer-term goals.

A time-related goal will usually answer these questions:

- When?
- What can I do six months from now? (or less depending on project length)
- What can I do six weeks from now? (or less depending on project length)
- What can I do today?

These were retrieved from reference [1]. For more detailed guidance on SMART planning, visit this resource.

## Level Specific Guidance

For more level specific guidance on the planning phase, visit the following documents depending on your level:

### Level 4

- [Communication](team-communication/level-4/level-4-team-communication-guidelines.md)
- [Project Planning](project-management/level-4/level-4-management-guidelines.md)
- [Version Control](version-control/level-4-git-instructions.md)

### Level 5

- [Communication](team-communication/level-5/level-5-team-communication-guidelines.md)
- [Project Planning](project-management/level-5/level-5-management-guidelines.md)
- [Version Control](version-control/level-5-git-instructions.md)

### Level 6

- [Communication](team-communication/level-6/level-6-team-communication-guidelines.md)
- [Project Planning](project-management/level-6/level-6-management-guidelines.md)
- [Version Control](version-control/level-6-git-instructions.md)

## 4. References

[1] MindTools. SMART Goals. <https://www.mindtools.com/pages/article/smart-goals.htm>.
