# Level 6 Team Communication Guidelines <!-- omit in toc -->

## 1. Abstract

This document will serve to provide students with advanced guidelines on how to communicate effectively with your team, so that you can use the concepts in your own projects and in the workplace after you graduate from university.

## 2. Table of Contents

- [1. Abstract](#1-abstract)
- [2. Table of Contents](#2-table-of-contents)
- [3. What Is Expected](#3-what-is-expected)
    - [**Microsoft Teams**](#microsoft-teams)
    - [**Zoom**](#zoom)
    - [**Cisco Webex**](#cisco-webex)
    - [**Gitter**](#gitter)
    - [**Element**](#element)
    - [**Discord**](#discord)
    - [**Slack**](#slack)
- [4. References](#4-references)
  
## 3. What Is Expected

At Level 6, you will have completed many projects involving some form of group work, even if it is just two of you. You should have experience using at least one communication tool as well as some experience with the **Scrum** framework.

At this level you should follow the Scrum framework whilst simultaniously communicating through one of the tools highlighted below when working remotely (preferably one of the top 5). If you follow this guidance, you will have the correct structure for effective communication, which will help mitigate many of the problems and risks that might arise during software development.

### **Microsoft Teams**

- Download/link: <https://www.microsoft.com/en/microsoft-teams/group-chat-software>
- How to use:  <https://support.microsoft.com/en-us/office/microsoft-teams-video-training-4f108e54-240b-4351-8084-b1089f0d21d7>
  
### **Zoom**

- Download/link: <https://zoom.us/>
- How to use: <https://support.zoom.us/hc/en-us/articles/206618765-Zoom-video-tutorials>
  
### **Cisco Webex**

- Download/link: <https://www.webex.com/unified-homepage-081220201.html>
- How to use: <https://help.webex.com/en-us/xqsxxt/Get-Started-with-Cisco-Webex-Training>
  
### **Gitter**

- Download/link: <https://gitter.im/>
- How to use: <https://www.youtube.com/watch?v=21cKayA2x0M>
  
### **Element**

- Download/link: <https://element.io/>
  
### **Discord**

- Download/link: <https://discord.com/>
- How to use: <https://support.discord.com/hc/en-us/articles/360045138571-Beginner-s-Guide-to-Discord>
  
### **Slack**

- Download/link: <https://slack.com/intl/en-gb/>
- How to use: <https://slack.com/intl/en-gb/resources/using-slack/how-to-use-slack>

It is also expected that you follow the Scrum framework (or something similar) and use it in conjunction with Kanban project managament. As a reminder, here is a diagram of the Scrum framework for you to follow. The framework is highlighted in detail in the [Level 5 Guidelines](../level-5/level-5-team-communication-guidelines.md):

![The Scrum Framework. By Scrum.org](../images/scrum-framework.png)
*Fig. 1: The structure of the Scrum framework. By Scrum.org.*

## 4. References

[1] Scrum.org. What is Scrum? <https://www.scrum.org/resources/what-is-scrum>.
