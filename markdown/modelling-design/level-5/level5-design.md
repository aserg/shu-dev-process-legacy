# Design - Level 5  

## 1. Introduction

The design step is focused on the modelling of the solution. It usually involves decisions around architecture, data structure, user-interface, classes, algorithms, etc. At Level 5, it is expected that you create diagrams for the architectural structure if those are required by your module, as well as detailed software model diagrams that link together well and are in sync. Previous description of what these diagrams are can be found [here](../level-4/level4-design.md). The three diagrams you need to create are:

- UML Class Diagram
- Entity Relationship Diagram
- Sequence Diagram

This document is a follow-up to the previous document and will provide examples of some more high level concepts you will need to consider when creating these diagrams. 

## 2. Table of Contents

- [Design - Level 5](#design---level-5)
    - [1. Introduction](#1-introduction)
    - [2. Table of Contents](#2-table-of-contents)
    - [3. Software Model Diagrams](#3-software-model-diagrams)
        - [3.1. UML Class Diagram](#31-uml-class-diagram)
        - [3.2. Entity Relationship Diagram](#32-entity-relationship-diagram)
        - [3.3. Sequence Diagram](#33-sequence-diagram)
        - [3.4 Architecture Overview](#34-architecture-overview)

## 3. Software Model Diagrams

### 3.1. UML Class Diagram

* [UML Class Diagram](level5-UML-class-diagram.md)

### 3.2. Entity Relationship Diagram

* [Entity Relationship Diagram](level5-entity-relationship-diagram.md)

### 3.3. Sequence Diagram

* [Sequence Diagram](level5-sequence-diagram.md)

### 3.4 Architecture Overview

You may also include an architecture overview, though it is optional at level 5 and you're not expected to document your architecture decisions.

* [Architecture Overview](../level-6/level6-architecture-overview.md)