# 3.3. Sequence Diagram - level 4 <!-- omit in toc -->

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Introduction](#introduction)
- [3.3.1. Sequence Diagram Notation](#331-sequence-diagram-notation)
- [3.3.2. Message and Focus of Control](#332-message-and-focus-of-control)
- [3.3.3. Sequence Fragments](#333-sequence-fragments)
- [References](#references)

## Introduction

This style of diagram describes the interactions among classes in terms of an exchange of messages over time. They are sometimes known as **event diagrams**. It is a good way to visualise and validate various runtime scenarios. These can help to predict how a system will behave and to discover responsibilities a class may need to have in the process of modeling a new system.

![Example sequence diagram of a hotel reservation system](https://cdn-images.visual-paradigm.com/guide/uml/what-is-sequence-diagram/01-sequence-diagram-example.png)  
*Figure 7: An example Sequence Diagram of a Hotel Reservation System*  

## 3.3.1. Sequence Diagram Notation

Visual Representations of any notation mentioned here can be found at [[3]](#4-references).

- **Actor**: This is a type of role by an entity that interacts with the subject. It represents roles as human users, external hardware or other subjects. It is important to note that an actor doesn't necessarily represent a specific physical entity but merely a particular role of some entity
- **Lifeline**: A lifeline represents an individual participant in the interaction
- **Call Message**: Call message is a kind of message that represents an invocation of operation of target lifeline
- **Return Message**: Return message is a kind of message that represents the pass of information back to the caller of a corresponded former message
- **Self Message**: Self message is a kind of message that represents the invocation of message of the same lifeline
- **Recursive Message**: Recursive message is a kind of message that represents the invocation of message of the same lifeline. It's target points to an activation on top of the activation where the message was invoked from
- **Create Message**: Create message is a kind of message that represents the instantiation of (target) lifeline
- **Destroy Message**: Destroy message is a kind of message that represents the request of destroying the lifecycle of target lifeline
- **Duration Message**: Duration message shows the distance between two time instants for a message invocation

## 3.3.2. Message and Focus of Control

An event is any point in an interaction where something occurs. Focus of control, also called execution occurrence, is shown as a tall, thin rectangle on a lifeline. It represents the period during which an element is performing an operation. The top and the bottom of the rectangle are aligned with the initiation and the completion time respectively.

![Example of message and focus of control](https://cdn-images.visual-paradigm.com/guide/uml/what-is-sequence-diagram/13-message-and-focus-of-control.png)  
*Figure 8: Visual example of message and focus of control*  

## 3.3.3. Sequence Fragments

UML 2.0 has helped to introduce sequence fragments, which make it easier to create and maintain accurate sequence diagrams. A sequence fragment is represented as a box, called a combined fragment, which encloses a portion of the interactions with a sequence diagram. TThe fragment operator indicates the type of fragment. More information on these types can be found at [[1]](#references).

![A sequence fragment](https://cdn-images.visual-paradigm.com/guide/uml/what-is-sequence-diagram/14-fragment.png)  
*Figure 9: A sequence fragment*

## References

- [1] Visual Paradigm: What is Sequence Diagram <https://www.visual-paradigm.com/guide/uml-unified-modeling-language/what-is-sequence-diagram/>  