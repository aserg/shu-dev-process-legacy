# 3.2. Entity Relationship Diagram - Level 4 <!-- omit in toc -->

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Introduction](#introduction)
- [3.2.1. Components and Features of ER Diagrams](#321-components-and-features-of-er-diagrams)
- [**Entity**](#entity)
- [**Relationship**](#relationship)
- [**Attribute**](#attribute)
- [**Cardinality**](#cardinality)
- [References](#references)

## Introduction

An Entity Relationship Diagram is a type of flowchart that illustrates how 'entities' such as people, objects or concepts relate to each other within a system. They are most often used to design or debug **relational databases** within software engineering, business information systems, education and research. Sometimes known as ERDs or ER Models, they use a defined set of symbols such as rectangles, diamonds, ovals and connecting lines to depict the interconnectedness of entities, relationships and their attributes.

![Entity relationship diagram for a school database](https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-school.svg)  
*Figure 6: Entity Relationship Diagram for a School Database*  

## 3.2.1. Components and Features of ER Diagrams

ER Diagrams are composed of **entities**, **relationships** and **attributes**. They also depict **cardinality**, which defines relationships in terms of numbers. Lets go through them.  

## **Entity**  

<p align = "center">
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-01.svg" class="no-original" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-02.svg" class="no-original"width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-03.svg" class="no-original"width="200" />
</p>  

This is a defineable thing, like a person, object, concept or event - that can have some data stored about it. You should think about entities as nouns. The shape of an entity is typically shown as a **rectangle**. The characteristics you need to know about Entities are.  
  
- **Entity Type:** A group of defineable things, such as athletes or students, whereas the entity would be the specific athlete or student.
- **Entity Set:** This is the same as an entity type, except it is defined at a particular point in time, such as students who enroll into a class. A related term is instance, in which the specific person or car would be an instance of the entity set.  
- **Entity Categories:** Entities are categorised as strong, weak or associative. A **strong entity** can be defined solely by it's own attributes, while a **weak entity** can't. An **associative entity** associates entities (or elements) within an entity set.  
- **Entity Keys:** This refers to an attribute that uniquely defines an entity in an entity set. Entity keys can be **Super**, **Candidate**, or **Primary**.
  - **Super Key:** A set of attributes (one or more) that together define an entity in an entity set
  - **Candidate Key:** A minimal key super key, meaning that it has the least possible number of attributes to still be a super key
  - **Primary Key:** A candidate key chosen by the database designer to uniquely identify the entity set
  - **Foreign Key:** Identifies the relationship between entities

## **Relationship**

<p align = "center">
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-09.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-10.svg" width="200" />
</p>  

This defines how entities act upon each other or how they are associated with one another. Think of them as verbs, e.g. a named student registers for a course, the two entities would be the student and the course, and the relationship between them would be that this student is enrolled on this course, depicted by the act of enrolment. Relationships are typically shown as **diamonds**. A **Recursive Relationship** describes when the same entity participates more than once in the relationship.

## **Attribute**

<p align = "center">
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-04.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-05.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-07.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-08.svg" width="200" />
</p>

An Attribute is defined as a property or characteristic of an entity. This is often shown as an **oval** or **circle**. A **Descriptive Attribute** is a property or characteristic of a relationship (versus of an entity). The categories of an attribute are.

- **Simple:** This means the attribute value is atomic (constant) and can't be further divided, such as a phone number
- **Composite:** Sub-attributes spring from an attribute
- **Derived:** Attributed is calculated or otherwsie derived from another sub attribute, such as age from a birthdate

## **Cardinality**

<p align = "center">
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-11.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-12.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-15.svg" width="200" />
  <img src="https://d2slcw3kip6qmk.cloudfront.net/marketing/pages/chart/seo/ERD/discovery/erd-symbols-16.svg" width="200" />
</p>

This defines the numerical attributes  of the relationship between two entities or entity sets. The three main cardinal relationships are.

- **one-to-one**: e.g. a student associated with one mailing list
- **one-to-many**: e.g. One student registers for multiple courses, but all those courses have a single line back to that one student
- **many-to-many**: e.g. Students as a group are associated with multiple faculty members, and faculty members in turn are associated with multiple students

Cardinality can be shown as look-across or same-side, depending on where the symbols are shown. The minimum or maximum numbers that apply to a relationship. More information on ER diagrams can be found at [[1]](#references).

## References

- [1] Lucidchart: What is an Entity Relationship Diagram? <https://www.lucidchart.com/pages/er-diagrams>