# Design - Level 4 <!-- omit in toc -->

## 1. Introduction

The design step is focused on the modelling of the solution. It usually involves decisions around architecture, data structure, user-interface, classes, algorithms, etc. At Level 4 it is not expected that you will create overly complex diagrams, more you will just need to create feasible basic diagrams that link well where necessary. The three most common diagrams you will expect to be creating are.

- UML Class Diagram
- Entity Relationship Diagram
- Sequence Diagram

This document will go through what these documents are and provide examples as we go through that you can look at to give you an idea what your document should look like. There will also be some blank templates you can use to get started. At Level 4 you only need to worry about **Inheritance** in your class diagrams. More on this later.

## 2. Table of Contents

- [1. Introduction](#1-introduction)
- [2. Table of Contents](#2-table-of-contents)
- [3. Diagrams That You Will Create](#3-diagrams-that-you-will-create)
    - [3.1. UML Class Diagram](#31-uml-class-diagram)
    - [3.2. Entity Relationship Diagram](#32-entity-relationship-diagram)
    - [3.3. Sequence Diagram](#33-sequence-diagram)
    - [3.4. Linking your Diagrams](#34-linking-your-diagrams)
- [4. References](#4-references)

## 3. Diagrams That You Will Create

### 3.1. UML Class Diagram

* [UML Class Diagram](../level4-UML-class-diagram)

### 3.2. Entity Relationship Diagram

* [Entity Relationship Diagram](../level4-entity-relationship-diagram)

### 3.3. Sequence Diagram

* [Sequence Diagrams](../level4-sequence-diagram)

### 3.4. Linking your Diagrams

When we use our class diagrams and sequence diagrams together, it allows for an extremely effective communication mechanism. We use a class diagram to illustrate relationships, and a sequence diagram to illustrate interactions between these classes. When an object is senidng information to another, this implies a relationship between the two classes. When thinking about how to link these together, if you've already created them beforehand, you can put them side-by-side and use this to help you understand what elements to take from each of them and then combine them into a single image. Examples to do this can be found at [[1]](#4-references).

## 4. References

- [1] Tech Republic: Class and Sequence diagrams work together to allow for precise modelling <https://www.techrepublic.com/article/class-and-sequence-diagrams-work-together-to-allow-precise-modeling/>
