# Design <!-- omit in toc -->

## Table of Contents

- [Table of Contents](#table-of-contents)
- [1. Introduction](#1-introduction)
- [2. Level Specific Guidance](#2-level-specific-guidance)
    - [Level 4](#level-4)
    - [Level 5](#level-5)
    - [Level 6](#level-6)

## 1. Introduction

The design step is focused on the modelling of the solution. It usually involves decision around architecture, data structure, user-interface, classes, algorithms, etc. The following will be covered:

- Techniques
  - Formal Diagrams
    - Class
    - Interaction/Sequence
  - Wireframes
  - Ad Hoc Drawing
  - Mindmaps
  - Lists  
- Finding what works for you
  - Capturing Ideas
  - Communicating
  - Revising
  - Tracking History
- Designing the
  - Architecture
  - Database
  - User Interface (UI)  

## 2. Level Specific Guidance

Once you have read the main document for each level, you can jump straight to specific diagram guidance below.
### [Level 4](level-4/level4-design.md)

* [UML Class Diagram](level-4/level4-UML-class-diagram)
* [Entity Relationship Diagram](level-4/level4-entity-relationship-diagram)
* [Sequence Diagrams](level-4/level4-sequence-diagram)
### [Level 5](level-5/level5-design.md)

* [UML Class Diagram](level-5/level5-UML-class-diagram.md)
* [Entity Relationship Diagram](level-5/level5-entity-relationship-diagram.md)
* [Sequence Diagrams](level-5/level5-sequence-diagram.md)

### [Level 6](level-6/level6-design.md)

* [Entity Relationship Diagram](level-6/level6-entity-relationship-diagram.md)
* [Sequence Diagrams](level-6/level6-sequence-diagram.md)
* [Architecture Overview](level-6/level6-architecture-overview.md)