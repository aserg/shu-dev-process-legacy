# Design - Level 6 <!-- omit in toc -->

## 1. Introduction

The design step is focused on the modelling of the solution. It usually involves decisions around architecture, data structure, user-interface, classes, algorithms, etc. At Level 6, it is expected that you create diagrams for the architectural structure if those are required by your module, as well as detailed software model diagrams that link together well and are in sync. You are also expected to create design models for your Final Year Project and group assignments. Previous description and explanation of what these diagrams are and how to create them can be found [here](../level-5/level5-design.md). The four diagrams you need to create are:

- UML Class Diagram
- Entity Relationship Diagram
- Sequence Diagram
- Architecture Overview

This document is a follow-up to the previous documents and will provide examples of some more high level concepts you will need to consider when creating these diagrams.

## 2. Table of Contents

- [1. Introduction](#1-introduction)
- [2. Table of Contents](#2-table-of-contents)
- [3. More Advanced Concepts](#3-more-advanced-concepts)
    - [3.1. Entity Relationship Diagrams](#31-entity-relationship-diagrams)
    - [3.2. Sequence Diagrams](#32-sequence-diagrams)
    - [3.3. Architecture Overview](#33-architecture-overview)

## 3. More Advanced Concepts

This section goes more into some more advanced notations that you can take advantage of in your ER and Sequence Diagrams, which will allow you to model most of the interactions that will take place in a common system, but also model some more complex interactions if you want to.

### 3.1. Entity Relationship Diagrams

* [Entity Relationship Diagram](level6-entity-relationship-diagram.md)

### 3.2. Sequence Diagrams

* [Sequence Diagrams](level6-sequence-diagram.md)

### 3.3. Architecture Overview

* [Architecture Overview](level6-architecture-overview.md)