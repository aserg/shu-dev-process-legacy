# Test <!-- omit in toc -->

## 1. Introduction

This phase is dedicated to testing the programs you have created during the construction-code phase. It is arguably a just as important phase as software with bugs in is not good software. Bugs can be anything from a simple typo, to critical, functionality impacting problems.

This series of documents will provide you with the knowledge of how to test your software effectively, a long with what sort of testing is expected from you at each level of study. On this course, you will be likely be using three types of testing:  

- Unit Testing
- Integration Testing
- User Acceptance Testing

Each of these will be covered in the relevant documents. The following will also be covered in this section:

- What should you test?
- Why do you test?
- Different approaches depending on level
- Testing within different development environments
- Automated testing using tools

## 2. Table of Contents

- [1. Introduction](#1-introduction)
- [2. Table of Contents](#2-table-of-contents)
- [Level Specific Guidance](#level-specific-guidance)

## Level Specific Guidance

* [Level 4](level-4/level4-testing.md)
* [Level 5](level-5/level5-testing.md)
* [Level 6](level-6/level6-testing.md)