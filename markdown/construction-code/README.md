# Code <!-- omit in toc -->

- [1. Introduction](#1-introduction)
- [Level Specific Guidance](#level-specific-guidance)
    - [Level 4](#level-4)
        - [Code Commenting](#code-commenting)
        - [Code Review](#code-review)
    - [Level 5](#level-5)
        - [Code Commenting](#code-commenting-1)
        - [Code Review](#code-review-1)
    - [Level 6](#level-6)
        - [Code Commenting](#code-commenting-2)
        - [Code Review](#code-review-2)

## 1. Introduction

The code step concerns the implementation of the structures modeled in the design phase. This phase is where development begins, and probably the main reason why you are on this course. Tutorials on how to code e.g. syntax of languages is not greatly covered in here, instead, good coding practices, good code comments, and how to do good code reviews are outlined and explained.

For guidance on coding standards on some languages you will likely encounter on the course (which should be adhered to in your projects as much as possible), visit either:

- [C#/C++ Coding Standards](coding-standards/c.md)
- [Java Coding Standards](coding-standards/java.md)
- [JavaScript Coding Standards](https://github.com/airbnb/javascript) (An example from AirBnb)
- [Python Coding Standards](coding-standards/python.md)
- [PHP Coding Standards](coding-standards/php.md)

## Level Specific Guidance

### Level 4

At level 4, your code should have comments that help you understand the code you've written. The comments should also help anyone else who consume your code but likely won't read through it in any depth. You're expected to work through manual code review techniques.

#### [Code Commenting](level-4/level4-code-commenting.md)
#### [Code Review](code-review/level-4/level-4-code-review-guidelines.md)

### Level 5

Your code comments should now help keep your code maintainable by following a consistent standard. You might consider automating your style checks to ensure consistency.

#### [Code Commenting](level-5/level5-code-commenting.md)
#### [Code Review](code-review/level-5/level-5-code-review-guidelines.md)

### Level 6

In your final year, you're expected to build on previous levels by also documenting the APIs you create.
#### [Code Commenting](level-6/level6-code-commenting.md)

- Should follow proper coding standards. Need to be consistent with design. Code should be properly documented and should have generated documentation like API doc (e.g., swagger) or Javadoc.
- A tool should be used, code coverage would be nice.

#### [Code Review](code-review/level-6/level-6-code-review-guidelines.md)
