# Software Development Process <!-- omit in toc -->

<a href="https://aserg.codeberg.page/shu-dev-process-legacy/" target="_blank" class=".mkdocs-hide" title="Shu Development Process">
    <img src="https://img.shields.io/badge/Documentation-View%20as%20HTML%20%E2%86%97-c10261" alt="View as HTML Badge"/>
</a>


# 1. Introduction

We are creating a software development process that can be followed by students at all levels as they work on both individual and group
assessments.
We're doing this because we want our students to use good practice, to understand the software lifecycle and to become familiar with industry-standard tools.


# 2. Table of Contents

- [1. Introduction](#1-introduction)
- [2. Table of Contents](#2-table-of-contents)
- [3. Setup](#3-setup)
- [4. Building](#4-building)
- [5. Deploying Content](#5-deploying-content)
- [6. License](#6-license)

# 3. Setup

1. Install the dependencies. You will need [mkdocs](https://www.mkdocs.org/) and [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) (VS Code extension).
    1. If you're editing the theme files in `rtd_theme_changes/` then you'll need to be familiar with [Jinja](https://jinja.palletsprojects.com/).
2. Clone this repository to your own machine: `git clone https://codeberg.org/aserg/shu-dev-process.git`.
3. Run `mkdocs serve` in the root of the repository to serve the documentation locally. This server has live-reload enabled.
4. Edit the content in `markdown/`. The file names and structure determine the published HTML path.
5. Optionally, edit the theme files in `rtd_theme_changes/`. We're building on the built-in [`readthedocs` theme](https://github.com/mkdocs/mkdocs/tree/master/mkdocs/themes/readthedocs) so files here overwrite the upstream files when documentation is built.
# 4. Building

Run `mkdocs build` in the root of this repository to build the HTML files manually. It will create a `docs/` folder with a structure matching the repository. No server is needed to view these files so you can actually start viewing the produced HTML through `docs/index.html`.

**Live server**

The `mkdocs serve` command can be run at the root of this repository to start a live server for working on the documentation at `http://localhost:8000/`. `serve-local.sh` will start this for you on port 8001. Every time you save a file, the server will reload the page you are looking at.

## 4.1. ReadTheDocs Theme Changes <!-- omit in toc -->

The default `readthedocs` theme is being used as the base for the rendered documentation. Using the [customization options](https://www.mkdocs.org/user-guide/customizing-your-theme/), changes have been made in `rtd_theme_changes/`:

- Styling to bring the site closer to SHU branding.
- PWA icons and search/social metadata.
- Service worker for asset caching.
- [MermaidJS](https://mermaid-js.github.io/) text to diagram support. You can use code blocks to quickly add a mermaid diagram. E.g.:
~~~
```mermaid
//your diagram text here
```
~~~
# 5. Deploying Content

Now you can deploy the website to [Codeberg Pages](https://docs.codeberg.org/codeberg-pages/) by running one of the deploy scripts:
## 5.1. Checking Content <!-- omit in toc -->

Run `deploy-develop.sh` to generate static HTML files from your current branch and automatically push them to the `pages-develop` branch. They'll then be served through at this address:

https://aserg.codeberg.page/shu-dev-process-legacy/@pages-develop/

## 5.2. Publishing New Content Online <!-- omit in toc -->

Run `deploy.sh` to generate static HTML files from the `main` branch and automatically push them to the `pages` branch. They'll then be served at this address:

https://aserg.codeberg.page/shu-dev-process-legacy/

**⚠ This address is distributed to students**, so content should be checked using the `deploy-develop` script before it is pushed to the live `pages` branch.
# 6. License

This material is made available under [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).