#!/usr/bin/env bash
# Run this script to deploy the current branch to Codeberg Pages pages-develop branch
branch=$(git branch | sed -n -e 's/^\* \(.*\)/\1/p')

./deploy.sh -s ${branch} -d pages-develop

read -p "Finished. Press any key to continue..."